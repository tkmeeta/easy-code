export default [
  {
    path: '/druid',
    title: '数据监控',
    name: 'druid',
    icon: 'ios-analytics',
    component: () => import('@/app/monitor/druid')
  },
  {
    path: '/monitor',
    title: '接口监控',
    name: 'monitor',
    icon: 'usb',
    component: () => import('@/app/monitor')
  }
]
