export default [
  {
    path: '/user',
    title: '人员管理',
    name: 'user',
    icon: 'ios-body',
    component: () => import('@/app/user')
  }
]
