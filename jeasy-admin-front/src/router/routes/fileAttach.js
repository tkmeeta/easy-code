export default [
  {
    path: '/fileAttach',
    title: '文件管理',
    name: 'fileAttach',
    icon: 'ios-folder',
    component: () => import('@/app/fileAttach')
  }
]
